FROM node:10-slim

RUN apt-get update -qq && apt-get install -y --no-install-recommends \
  tzdata \
  python \
  git \
  build-essential

RUN mkdir /front -p
WORKDIR /front

ADD package.json /front/package.json

COPY . /front

RUN npm install -g @angular/cli
RUN npm install --silent

# The default port from ng serve (4200)
# and 49153 for Webpack Hot Module Reload.
EXPOSE 4200 49153 
